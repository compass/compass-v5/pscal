
# Comments

* on fait beaucoup de fois la même chose dans les op de variables
  * en vrai on fait des op sur Scalar/Vector
  * => autant implémenter les op sur Scalar/Vector et s'en servir !


# Content

## State

- un struct qui fonctionne comme un aggrégat avec un type unique
- attribut de classe pour indiquer le type `value_t`
- conversion vers et depuis (?) un std::array

## Variable

- calculable depuis une valeur de `State`
- valeur scalaire : `State::value_t`
- dérivable par rapport à chaque valeur de state
<!-- - fournie par une fonction `f(state, df) -> value` -->
- fournie par une fonction `f(state) -> [value, df]`
  - VariableFunction ?
- extension : valeur vectoriel
  - en plus de `f(State x) -> [State::value_t y, State dy]`
  - en prend aussi `f(std::array<State, N> x) -> [std::array<State::value_t, N> y, std::array<State, N> dy]`
    - peut-être un type dédié : MultiVariable ?

Et si on faisait le lien avec le State de référence dès la création de Variable ?
- on internalise le SubState plutôt que de l'appeler explicitement
- il faut quand même y référer pour créer correctement une Variable
- conséquences:
  - les Variable sont sur le même State de référence, même si la function sous-jacente est sur un State différent
  - on peut assembler les Variables entre elles sans plus de questions ensuite.
  - produit directement la fonction sur le State de référence


## Accessor

- comme `Variable` mais en fait non
- non car ça serait plutôt une fonction
- **pour plus tard**
- **/!\  ON CHANGE**
  - en fait c'est juste une Variable

### SiteVariable
- notion extérieure à pscal
- croisement de Variable avec les sites, la géométrie et la collection des states
- ce qui est utilisé dans le schéma

## SubState -> StateMapping <-

- un struct extrait de state
- une application (linéaire) qui transforme une dérivée sur le sub-state en une dérivée sur le state
- pseudo-code:
    X = ...
    Xa = E(X).v
    [y, dydXa] = f(Xa)
    dydX = E(X).d(dydXa)
    # (y, dydXa) selon Xa
    # (y, dydX) selon X
- marche aussi bien avec l'overloading pour avoir un seul nom applicable à plusieurs type de state


# How

## Eigen

- eigen fait du "expression template"
- idée : prendre la dépendence eigen et simplifier le code
- quelques ref pour l'intégration
  - https://gitlab.brgm.fr/brgm/modelisation-geologique/compass/icus/-/commit/e7bbeb60a358f56c660c5e5eddb8bfec0169c641#74b56595278fe3458a4c2c9d26611f405050ba7b_0_1
  - https://gitlab.brgm.fr/brgm/modelisation-geologique/compass/using-eigen/-/blob/main/test/using-eigen.cpp

## Héritage vs fonctionnel

- j'ai fait un 1er test functionnel
- on peut faire de l'héritage
  - nécessite le "Curiously recurring template pattern"
    - (c'est pour ça que mon 1er essai à raté)
  - on peut "activer des comportements" dans un bloc de code
    - "using pscal::state::my-behavior"
    - permet de cacher moult choses !
  - on peut connecter avec as/from_array avec le casting
    - https://en.cppreference.com/w/cpp/language/cast_operator
