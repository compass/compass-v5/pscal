This directory contains yaml files to set
[conda environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#managing-environments).
To use them just run:

```shell
conda env create -f file.yml [-n my_fancy_name]
```
