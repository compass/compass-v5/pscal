#include <cassert>
#include <type_traits>

#include "pscal/math.h"
#include "pscal/state.h"
#include "pscal/utils.h"

struct MyState : pscal::State<double, MyState> {
  value_t x;
  value_t y;
  value_t z[2];
};

double f(const MyState &x, MyState &dx) {
  dx.x = 1;
  dx.y = 1;
  dx.z[0] = 1;
  return x.x + x.y + x.z[0];
}

double g(const MyState &x, MyState &dx) {
  dx.z[0] = x.z[1];
  dx.z[1] = x.z[0];
  return x.z[0] * x.z[1];
}

void test_more() {
  MyState dy{};
  MyState x;
  x.as_array() = {1, 2, 3, 4};

  using V = pscal::math::func::Variable<MyState>;

  auto F = V(f);
  auto G = V(g);

  auto h = ((F + G) * F + G) * F;
  auto y = h.func(x, dy);

  std::cout << y << " " << dy.x << " " << dy.y << " " << dy.z[0] << " "
            << dy.z[1] << "\n";
}

void test_add() {
  MyState dy{};
  MyState x;
  x.as_array() = {1, 2, 3, 4};

  using V = pscal::math::func::Variable<MyState>;

  auto h = V(f) + V(g);
  auto y = h.func(x, dy);

  assert(y == 18);
  assert(dy.x == 1);
  assert(dy.y == 1);
  assert(dy.z[0] == 5);
  assert(dy.z[1] == 3);
}

void test_mul() {
  MyState dy{};
  MyState x;
  x.as_array() = {1, 2, 3, 4};

  using V = pscal::math::func::Variable<MyState>;

  auto h = V(f) * V(g);
  auto y = h.func(x, dy);

  assert(y == 72);
  assert(dy.x == 12);
  assert(dy.y == 12);
  assert(dy.z[0] == 36);
  assert(dy.z[1] == 18);

  auto hh = h * 2;
  y = hh.func(x, dy);

  assert(y == 144);
  assert(dy.x == 24);
  assert(dy.y == 24);
  assert(dy.z[0] == 72);
  assert(dy.z[1] == 36);

  hh = 2 * h;
  y = hh.func(x, dy);

  assert(y == 144);
  assert(dy.x == 24);
  assert(dy.y == 24);
  assert(dy.z[0] == 72);
  assert(dy.z[1] == 36);
}

int main() {
  test_add();
  test_mul();
  test_more();
}
