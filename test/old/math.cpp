#include "pscal/math.h"
#include "pscal/state.h"
#include "pscal/utils.h"
#include <cassert>

struct MyState : pscal::State<double, MyState> {
  value_t x;
  value_t y;
  value_t z[2];
};

void test_add() {
  using namespace pscal::math;

  MyState x;
  x.as_array() = {10, 20, 30, 40};
  MyState dx;
  dx.as_array() = {1, 2, 3, 4};

  x += dx;
  assert(x.x == 11);
  assert(x.y == 22);
  assert(x.z[0] == 33);
  assert(x.z[1] == 44);

  auto y = x + dx;
  assert(y.x == 12);
  assert(y.y == 24);
  assert(y.z[0] == 36);
  assert(y.z[1] == 48);
}

void test_mul() {
  using namespace pscal::math;

  MyState x;
  x.as_array() = {10, 20, 30, 40};
  MyState dx;
  dx.as_array() = {1, 2, 3, 4};

  x *= dx;
  assert(x.x == 10);
  assert(x.y == 40);
  assert(x.z[0] == 90);
  assert(x.z[1] == 160);

  auto y = x * dx;
  assert(y.x == 10);
  assert(y.y == 80);
  assert(y.z[0] == 270);
  assert(y.z[1] == 640);

  x *= 0.1;
  assert(x.x == 1);
  assert(x.y == 4);
  assert(x.z[0] == 9);
  assert(x.z[1] == 16);

  y = x * 2;
  assert(y.x == 2);
  assert(y.y == 8);
  assert(y.z[0] == 18);
  assert(y.z[1] == 32);

  y = 2 * x;
  assert(y.x == 2);
  assert(y.y == 8);
  assert(y.z[0] == 18);
  assert(y.z[1] == 32);
}

void test_div() {
  using namespace pscal::math;

  MyState x;
  x.as_array() = {10, 20, 80, 40};
  MyState dx;
  dx.as_array() = {1, 2, 8, 4};

  x /= dx;
  assert(x.x == 10);
  assert(x.y == 10);
  assert(x.z[0] == 10);
  assert(x.z[1] == 10);

  auto y = dx / x;
  assert(y.x == 0.1);
  assert(y.y == 0.2);
  assert(y.z[0] == 0.8);
  assert(y.z[1] == 0.4);

  y /= 0.1;
  assert(y.x == 1);
  assert(y.y == 2);
  assert(y.z[0] == 8);
  assert(y.z[1] == 4);

  y = y * 2;
  assert(y.x == 2);
  assert(y.y == 4);
  assert(y.z[0] == 16);
  assert(y.z[1] == 8);

  y = y * 2;
  assert(y.x == 4);
  assert(y.y == 8);
  assert(y.z[0] == 32);
  assert(y.z[1] == 16);
}

/* void test_expr() { */
/*   using namespace pscal::math ; */

/*   MyState a {1, 2, 3, 4}; */
/*   auto e = expr::Value(a); */
/*   assert(e.array[0] == 1); */
/*   a.x = 10; */
/*   assert(e.array[0] == 10); */
/*   e.array[0] = 100; */
/*   assert(a.x == 100); */

/*   auto aa = eval(e) ; */
/*   assert(aa.x == 100); */

/*   MyState b {10, 20, 30, 40}; */
/*   auto f = expr::Value(b); */
/*   auto c = expr::eval(e + f) ; */
/*   assert(c.x == 110); */
/*   assert(c.y == 22); */
/*   assert(c.z[0] == 33); */
/*   assert(c.z[1] == 44); */

/*   pscal::utils::print(a); */
/*   pscal::utils::print(eval(e)); */
/*   pscal::utils::print(eval(e+f)); */
/*   pscal::utils::print(eval(e+f+f)); */
/*   pscal::utils::print(eval(e*f+f)); */
/*   pscal::utils::print(eval((e+f)*f)); */
/*   pscal::utils::print(eval((e+f)+f+e)); */
/*   pscal::utils::print(eval((e+f)*(f+e))); */
/*   /1* pscal::utils::print(eval((e+f)*f+e));  // error *1/ */

/*   std::cout<< ((e+f)*f+e)[0] <<"\n"; */
/*   /1* MyState q = eval((e+f)*f+e) ;  // error *1/ */

/*   std::cout<<e[0]<<" "<<e[1]<<" "<<e[2]<<" "<<e[3]<<"\n"; */
/*   std::cout<<f[0]<<" "<<f[1]<<" "<<f[2]<<" "<<f[3]<<"\n"; */

/*   auto z = (e+f)*f+e ; */
/*   double z_0 = z[0]; */
/*   /1* std::cout<<z[0]<<"\n";  // error *1/ */
/*   /1* std::cout<<z[0]<<" "<<z[1]<<" "<<z[2]<<" "<<z[3]<<"\n";  // error *1/
 */
/*   /1* z/1; *1/ */
/*   MyState z_val = eval(z) ; */
/*   auto& z_arr = pscal::as_array(z_val); */
/*   z_arr[0]; */
/*   /1* std::cout<<z_arr[0]<<"\n";  // error *1/ */
/*   z_val.x; */
/*   /1* std::cout<<z_val.x<<"\n";  // error *1/ */
/*   double z_val_x = z_val.x; */
/*   /1* std::cout<<z_val_x<<"\n";  // error *1/ */
/*   /1* pscal::utils::print(z_val);  // error *1/ */
/* } */

int main() {
  test_add();
  test_mul();
  test_div();
}
