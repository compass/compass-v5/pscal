#include <catch2/catch_test_macros.hpp>
#include <vector>

#include "pscal/multistate-operators.h"

struct X {
  using value_t = double;
  double x, y;
};

struct MyValue {
  X::value_t value;
  std::vector<X> derivatives;
};

namespace fake42 {
template <typename T> T &operator+=(T &a, const T) {
  a.value = 42;
  return a;
}
} // namespace fake42

TEST_CASE("multi-state operators") {
  STATIC_CHECK(pscal::State<X>);
  STATIC_CHECK(pscal::multistate::Scalar<MyValue>);

  {
    MyValue a = {-1, {{.x = 1, .y = 2}, {.x = 3, .y = 4}}};
    MyValue b = {-10, {{.x = 10, .y = 20}, {.x = 30, .y = 40}}};

    CHECK(a.value == -1);
    CHECK(a.derivatives[0].x == 1);
    CHECK(a.derivatives[0].y == 2);
    CHECK(a.derivatives[1].x == 3);
    CHECK(a.derivatives[1].y == 4);
  }

  {
    using namespace fake42;
    MyValue a = {-1, {{.x = 1, .y = 2}, {.x = 3, .y = 4}}};
    MyValue b = {-10, {{.x = 10, .y = 20}, {.x = 30, .y = 40}}};
    a += b;
    CHECK(a.value == 42);
  }

  {
    using namespace pscal::multistate::operators;
    MyValue a = {-1, {{.x = 1, .y = 2}, {.x = 3, .y = 4}}};
    MyValue b = {-10, {{.x = 10, .y = 20}, {.x = 30, .y = 40}}};
    a += b;
    CHECK(a.value == -11);
    CHECK(a.derivatives[0].x == 11);
    CHECK(a.derivatives[0].y == 22);
    CHECK(a.derivatives[1].x == 33);
    CHECK(a.derivatives[1].y == 44);
  }

  {
    using namespace pscal::multistate::operators;
    MyValue a = {-1, {{.x = 1, .y = 2}, {.x = 3, .y = 4}}};
    MyValue b = {-10, {{.x = 10, .y = 20}, {.x = 30, .y = 40}}};
    a -= b;
    CHECK(a.value == 9);
    CHECK(a.derivatives[0].x == -9);
    CHECK(a.derivatives[0].y == -18);
    CHECK(a.derivatives[1].x == -27);
    CHECK(a.derivatives[1].y == -36);
  }

  {
    using namespace pscal::multistate::operators;
    MyValue a = {-1, {{.x = 1, .y = 2}, {.x = 3, .y = 4}}};
    a *= 10;
    CHECK(a.value == -10);
    CHECK(a.derivatives[0].x == 10);
    CHECK(a.derivatives[0].y == 20);
    CHECK(a.derivatives[1].x == 30);
    CHECK(a.derivatives[1].y == 40);
  }

  /* { // non matching numbers of states */
  /*   using namespace pscal::multistate::operators; */
  /*   MyValue a = {-1, {{.x = 1, .y = 2}, {.x = 3, .y = 4}, {.x = 5, .y = 6}}};
   */
  /*   MyValue b = {-10, {{.x = 10, .y = 20}, {.x = 30, .y = 40}}}; */
  /*   a += b; */
  /*   CHECK(a.value == -11); */
  /*   CHECK(a.derivatives[0].x == 11); */
  /*   CHECK(a.derivatives[0].y == 22); */
  /*   CHECK(a.derivatives[1].x == 33); */
  /*   CHECK(a.derivatives[1].y == 44); */
  /*   CHECK(a.derivatives[2].x == 5); */
  /*   CHECK(a.derivatives[2].y == 6); */
  /* } */
}
