#include <array>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <tuple>

#include "pscal/state.h"
#include "pscal/value.h"
#include "pscal/variable.h"

using namespace Catch::Matchers;

namespace {
struct X {
  using value_t = double;
  double x, y, z;
};

struct Y {
  using value_t = double;
  double x, y;
};

struct GetY {
  using From = X;
  using To = Y;
  To v;
  GetY(const From &x) : v{.x = x.x, .y = x.y} {}
  From d(const To &dy) { return {.x = dy.x, .y = dy.y, .z = 0}; }
};

auto f(const Y &state) {
  return pscal::Scalar{state.x + 2 * state.y, Y{.x = 1, .y = 2}};
}

auto g(const Y &state) {
  return pscal::Scalar{state.x * state.y, Y{.x = state.y, .y = state.x}};
}

auto vect(const Y &state) {
  return pscal::Vector<Y, 3>{
      {state.x + 2 * state.y, state.x + 3 * state.y, 5 * state.x + state.y},
      {Y{.x = 1, .y = 2}, Y{.x = 1, .y = 3}, Y{.x = 5, .y = 1}}};
}
} // namespace

TEST_CASE("variable") {
  using namespace pscal;

  SECTION("test explo") {
    auto [y, dy] = f(Y{.x = 10, .y = 100});
    CHECK(y == 210);
    CHECK(dy.x == 1);
    CHECK(dy.y == 2);

    auto y_var = f(Y{.x = 10, .y = 100});
    CHECK(y_var.value == 210);
    CHECK(y_var.derivatives.x == 1);
    CHECK(y_var.derivatives.y == 2);

    auto toto = Y{.x = 1, .y = 0};
    CHECK(toto.x == 1);
    CHECK(toto.y == 0);

    X x{.x = 1, .y = 2, .z = 3};
    auto E = GetY(x);
    CHECK(E.v.x == 1);
    CHECK(E.v.y == 2);
    CHECK(E.d(dy).x == 1);
    CHECK(E.d(dy).y == 2);
    CHECK(E.d(dy).z == 0);

    using namespace pscal;
    using VXY = Variable<GetY>;
    auto myfunc1 = VXY::Func<Scalar<Y>>(
        [](const auto &state) { return Scalar{1, state}; });
    VXY::Func<Scalar<Y>> myfunc2 = [](const auto &state) {
      return Scalar{1, state};
    };
    auto myfunc3 =
        VXY::Func<Scalar<Y>>([](auto state) { return Scalar{1, state}; });
    auto myfunc4 = VXY::Func<Scalar<Y>>([](auto state) {
      state.x = 10;
      return Scalar{1, state};
    });

    auto yy = Y{.x = 1, .y = 2};
    auto [aa, daa] = myfunc4(yy);
    CHECK(yy.x == 1);
    CHECK(daa.x == 10);

    STATIC_CHECK(StateMapping<GetY>);

    auto v = Variable<GetY>(f);
    auto fv = v._func(Y{.x = 1, .y = 2});
    CHECK(fv.value == 5);
    CHECK(fv.derivatives.x == 1);
    CHECK(fv.derivatives.y == 2);
    auto vv = v(X{.x = 1, .y = 2, .z = 3});
    CHECK(vv.value == 5);
    CHECK(vv.derivatives.x == 1);
    CHECK(vv.derivatives.y == 2);
    CHECK(vv.derivatives.z == 0);

    // explicit conversion
    // double vv_val = double(vv);
    // CHECK(vv_val == 5);
    CHECK(double(vv) == 5);

    auto var_vect = Variable<GetY, 3>(vect);
    auto val_vect = var_vect(X{.x = 1, .y = 2, .z = 3});
    CHECK(val_vect.value[0] == 5);
    CHECK(val_vect.value[1] == 7);
    CHECK(val_vect.value[2] == 7);
    CHECK(val_vect.derivatives[0].x == 1);
    CHECK(val_vect.derivatives[0].y == 2);
    CHECK(val_vect.derivatives[0].z == 0);
    CHECK(val_vect.derivatives[1].x == 1);
    CHECK(val_vect.derivatives[1].y == 3);
    CHECK(val_vect.derivatives[1].z == 0);
    CHECK(val_vect.derivatives[2].x == 5);
    CHECK(val_vect.derivatives[2].y == 1);
    CHECK(val_vect.derivatives[2].z == 0);
  }

  SECTION("test_identity") {
    auto f = Variable<IdMap<X>>([](X x) {
      return Scalar<X>{x.x * x.y * x.z,
                       X{.x = x.y * x.z, .y = x.x * x.z, .z = x.x * x.y}};
    });
    auto x = f(X{.x = 1, .y = 20, .z = 300});
    CHECK(x.value == 6000);
    CHECK(x.derivatives.x == 6000);
    CHECK(x.derivatives.y == 300);
    CHECK(x.derivatives.z == 20);
  }

  SECTION("test_add") {
    auto var_f = Variable<GetY>(f);
    auto var_g = Variable<GetY>(g);
    auto var_h = Variable<IdMap<X>>(
        [](X x) { return Scalar<X>{2 * x.z, X{.x = 0, .y = 0, .z = 2}}; });
    auto var_vect = Variable<GetY, 3>(vect);
    auto var_vect_ix = Variable<IdMap<X>, 3>([](X x) {
      return Vector<X, 3>{{x.z, x.x, x.y},
                          {X{.x = 0, .y = 0, .z = 1}, X{.x = 1, .y = 0, .z = 0},
                           X{.x = 0, .y = 1, .z = 0}}};
    });

    { // scalar + scalar same mapping
      auto var_sum = var_f + var_g;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 61);
      CHECK(v.derivatives.x == 21);
      CHECK(v.derivatives.y == 3);
      CHECK(v.derivatives.z == 0);
    }

    { // scalar + scalar different mapping
      auto var_sum = var_f + var_h;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 641);
      CHECK(v.derivatives.x == 1);
      CHECK(v.derivatives.y == 2);
      CHECK(v.derivatives.z == 2);
    }

    { // scalar + const
      auto var_sum = var_f + 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 1041);
      CHECK(v.derivatives.x == 1);
      CHECK(v.derivatives.y == 2);
      CHECK(v.derivatives.z == 0);
    }

    { // const + scalar
      auto var_sum = 1000 + var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 1041);
      CHECK(v.derivatives.x == 1);
      CHECK(v.derivatives.y == 2);
      CHECK(v.derivatives.z == 0);
    }

    { // vector + vector same mapping
      auto var_sum = var_vect + var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 82);
      CHECK(v.value[1] == 122);
      CHECK(v.value[2] == 50);
      CHECK(v.derivatives[0].x == 2);
      CHECK(v.derivatives[0].y == 4);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 2);
      CHECK(v.derivatives[1].y == 6);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 10);
      CHECK(v.derivatives[2].y == 2);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector + vector different mapping
      auto var_sum = var_vect + var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 341);
      CHECK(v.value[1] == 62);
      CHECK(v.value[2] == 45);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == 1);
      CHECK(v.derivatives[1].x == 2);
      CHECK(v.derivatives[1].y == 3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5);
      CHECK(v.derivatives[2].y == 2);
      CHECK(v.derivatives[2].z == 0);
    }

    { // scalar + vector different mapping
      auto var_sum = var_f + var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 341);
      CHECK(v.value[1] == 42);
      CHECK(v.value[2] == 61);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == 1);
      CHECK(v.derivatives[1].x == 2);
      CHECK(v.derivatives[1].y == 2);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 1);
      CHECK(v.derivatives[2].y == 3);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector + scalar different mapping
      auto var_sum = var_vect_ix + var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 341);
      CHECK(v.value[1] == 42);
      CHECK(v.value[2] == 61);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == 1);
      CHECK(v.derivatives[1].x == 2);
      CHECK(v.derivatives[1].y == 2);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 1);
      CHECK(v.derivatives[2].y == 3);
      CHECK(v.derivatives[2].z == 0);
    }

    { // scalar + vector same mapping
      auto var_sum = var_f + var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 82);
      CHECK(v.value[1] == 102);
      CHECK(v.value[2] == 66);
      CHECK(v.derivatives[0].x == 2);
      CHECK(v.derivatives[0].y == 4);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 2);
      CHECK(v.derivatives[1].y == 5);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 6);
      CHECK(v.derivatives[2].y == 3);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector + scalar same mapping
      auto var_sum = var_vect + var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 82);
      CHECK(v.value[1] == 102);
      CHECK(v.value[2] == 66);
      CHECK(v.derivatives[0].x == 2);
      CHECK(v.derivatives[0].y == 4);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 2);
      CHECK(v.derivatives[1].y == 5);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 6);
      CHECK(v.derivatives[2].y == 3);
      CHECK(v.derivatives[2].z == 0);
    }

    { // const + vector same mapping
      auto var_sum = 1000 + var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 1041);
      CHECK(v.value[1] == 1061);
      CHECK(v.value[2] == 1025);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 1);
      CHECK(v.derivatives[1].y == 3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5);
      CHECK(v.derivatives[2].y == 1);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector + const same mapping
      auto var_sum = var_vect + 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 1041);
      CHECK(v.value[1] == 1061);
      CHECK(v.value[2] == 1025);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 1);
      CHECK(v.derivatives[1].y == 3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5);
      CHECK(v.derivatives[2].y == 1);
      CHECK(v.derivatives[2].z == 0);
    }
  }

  SECTION("test_subtract") {
    auto var_f = Variable<GetY>(f);
    auto var_g = Variable<GetY>(g);
    auto var_h = Variable<IdMap<X>>(
        [](X x) { return Scalar<X>{2 * x.z, X{.x = 0, .y = 0, .z = 2}}; });
    auto var_vect = Variable<GetY, 3>(vect);
    auto var_vect_ix = Variable<IdMap<X>, 3>([](X x) {
      return Vector<X, 3>{{x.z, x.x, x.y},
                          {X{.x = 0, .y = 0, .z = 1}, X{.x = 1, .y = 0, .z = 0},
                           X{.x = 0, .y = 1, .z = 0}}};
    });

    { // scalar - scalar same mapping
      auto var_sum = var_f - var_g;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 21);
      CHECK(v.derivatives.x == -19);
      CHECK(v.derivatives.y == 1);
      CHECK(v.derivatives.z == 0);
    }

    { // scalar - scalar different mapping
      auto var_sum = var_f - var_h;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == -559);
      CHECK(v.derivatives.x == 1);
      CHECK(v.derivatives.y == 2);
      CHECK(v.derivatives.z == -2);
    }

    { // scalar - const
      auto var_sum = var_f - 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == -959);
      CHECK(v.derivatives.x == 1);
      CHECK(v.derivatives.y == 2);
      CHECK(v.derivatives.z == 0);
    }

    { // const - scalar
      auto var_sum = 1000 - var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 959);
      CHECK(v.derivatives.x == -1);
      CHECK(v.derivatives.y == -2);
      CHECK(v.derivatives.z == 0);
    }

    { // vector - vector same mapping
      auto var_sum = var_vect - var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 0);
      CHECK(v.value[1] == 0);
      CHECK(v.value[2] == 0);
      CHECK(v.derivatives[0].x == 0);
      CHECK(v.derivatives[0].y == 0);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == 0);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 0);
      CHECK(v.derivatives[2].y == 0);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector - vector different mapping
      auto var_sum = var_vect - var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == -259);
      CHECK(v.value[1] == 60);
      CHECK(v.value[2] == 5);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == -1);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == 3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5);
      CHECK(v.derivatives[2].y == 0);
      CHECK(v.derivatives[2].z == 0);
    }

    { // scalar - vector different mapping
      auto var_sum = var_f - var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == -259);
      CHECK(v.value[1] == 40);
      CHECK(v.value[2] == 21);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == -1);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == 2);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 1);
      CHECK(v.derivatives[2].y == 1);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector - scalar different mapping
      auto var_sum = var_vect_ix - var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 259);
      CHECK(v.value[1] == -40);
      CHECK(v.value[2] == -21);
      CHECK(v.derivatives[0].x == -1);
      CHECK(v.derivatives[0].y == -2);
      CHECK(v.derivatives[0].z == 1);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == -2);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == -1);
      CHECK(v.derivatives[2].y == -1);
      CHECK(v.derivatives[2].z == 0);
    }

    { // scalar - vector same mapping
      auto var_sum = var_f - var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 0);
      CHECK(v.value[1] == -20);
      CHECK(v.value[2] == 16);
      CHECK(v.derivatives[0].x == 0);
      CHECK(v.derivatives[0].y == 0);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == -1);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == -4);
      CHECK(v.derivatives[2].y == 1);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector - scalar same mapping
      auto var_sum = var_vect - var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 0);
      CHECK(v.value[1] == 20);
      CHECK(v.value[2] == -16);
      CHECK(v.derivatives[0].x == 0);
      CHECK(v.derivatives[0].y == 0);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == 1);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 4);
      CHECK(v.derivatives[2].y == -1);
      CHECK(v.derivatives[2].z == 0);
    }

    { // const - vector same mapping
      auto var_sum = 1000 - var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 959);
      CHECK(v.value[1] == 939);
      CHECK(v.value[2] == 975);
      CHECK(v.derivatives[0].x == -1);
      CHECK(v.derivatives[0].y == -2);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == -1);
      CHECK(v.derivatives[1].y == -3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == -5);
      CHECK(v.derivatives[2].y == -1);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector - const same mapping
      auto var_sum = var_vect - 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == -959);
      CHECK(v.value[1] == -939);
      CHECK(v.value[2] == -975);
      CHECK(v.derivatives[0].x == 1);
      CHECK(v.derivatives[0].y == 2);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 1);
      CHECK(v.derivatives[1].y == 3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5);
      CHECK(v.derivatives[2].y == 1);
      CHECK(v.derivatives[2].z == 0);
    }
  }

  SECTION("test_multiply") {
    auto var_f = Variable<GetY>(f);
    auto var_g = Variable<GetY>(g);
    auto var_h = Variable<IdMap<X>>(
        [](X x) { return Scalar<X>{2 * x.z, X{.x = 0, .y = 0, .z = 2}}; });
    auto var_vect = Variable<GetY, 3>(vect);
    auto var_vect_ix = Variable<IdMap<X>, 3>([](X x) {
      return Vector<X, 3>{{x.z, x.x, x.y},
                          {X{.x = 0, .y = 0, .z = 1}, X{.x = 1, .y = 0, .z = 0},
                           X{.x = 0, .y = 1, .z = 0}}};
    });

    { // scalar * scalar same mapping
      auto var_sum = var_f * var_g;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 820);
      CHECK(v.derivatives.x == 840);
      CHECK(v.derivatives.y == 81);
      CHECK(v.derivatives.z == 0);
    }

    { // scalar * scalar different mapping
      auto var_sum = var_f * var_h;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 24600);
      CHECK(v.derivatives.x == 600);
      CHECK(v.derivatives.y == 1200);
      CHECK(v.derivatives.z == 82);
    }

    { // scalar * const
      auto var_sum = var_f * 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 41000);
      CHECK(v.derivatives.x == 1000);
      CHECK(v.derivatives.y == 2000);
      CHECK(v.derivatives.z == 0);
    }

    { // const * scalar
      auto var_sum = 1000 * var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 41000);
      CHECK(v.derivatives.x == 1000);
      CHECK(v.derivatives.y == 2000);
      CHECK(v.derivatives.z == 0);
    }

    { // vector * vector same mapping
      auto var_sum = var_vect * var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 1681);
      CHECK(v.value[1] == 3721);
      CHECK(v.value[2] == 625);
      CHECK(v.derivatives[0].x == 82);
      CHECK(v.derivatives[0].y == 164);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 122);
      CHECK(v.derivatives[1].y == 366);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 250);
      CHECK(v.derivatives[2].y == 50);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector * vector different mapping
      auto var_sum = var_vect * var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 12300);
      CHECK(v.value[1] == 61);
      CHECK(v.value[2] == 500);
      CHECK(v.derivatives[0].x == 300);
      CHECK(v.derivatives[0].y == 600);
      CHECK(v.derivatives[0].z == 41);
      CHECK(v.derivatives[1].x == 62);
      CHECK(v.derivatives[1].y == 3);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 100);
      CHECK(v.derivatives[2].y == 45);
      CHECK(v.derivatives[2].z == 0);
    }

    { // scalar * vector different mapping
      auto var_sum = var_f * var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 12300);
      CHECK(v.value[1] == 41);
      CHECK(v.value[2] == 820);
      CHECK(v.derivatives[0].x == 300);
      CHECK(v.derivatives[0].y == 600);
      CHECK(v.derivatives[0].z == 41);
      CHECK(v.derivatives[1].x == 42);
      CHECK(v.derivatives[1].y == 2);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 20);
      CHECK(v.derivatives[2].y == 81);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector * scalar different mapping
      auto var_sum = var_vect_ix * var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 12300);
      CHECK(v.value[1] == 41);
      CHECK(v.value[2] == 820);
      CHECK(v.derivatives[0].x == 300);
      CHECK(v.derivatives[0].y == 600);
      CHECK(v.derivatives[0].z == 41);
      CHECK(v.derivatives[1].x == 42);
      CHECK(v.derivatives[1].y == 2);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 20);
      CHECK(v.derivatives[2].y == 81);
      CHECK(v.derivatives[2].z == 0);
    }

    { // scalar * vector same mapping
      auto var_sum = var_f * var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 1681);
      CHECK(v.value[1] == 2501);
      CHECK(v.value[2] == 1025);
      CHECK(v.derivatives[0].x == 82);
      CHECK(v.derivatives[0].y == 164);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 102);
      CHECK(v.derivatives[1].y == 245);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 230);
      CHECK(v.derivatives[2].y == 91);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector * scalar same mapping
      auto var_sum = var_vect * var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 1681);
      CHECK(v.value[1] == 2501);
      CHECK(v.value[2] == 1025);
      CHECK(v.derivatives[0].x == 82);
      CHECK(v.derivatives[0].y == 164);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 102);
      CHECK(v.derivatives[1].y == 245);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 230);
      CHECK(v.derivatives[2].y == 91);
      CHECK(v.derivatives[2].z == 0);
    }

    { // const * vector same mapping
      auto var_sum = 1000 * var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 41000);
      CHECK(v.value[1] == 61000);
      CHECK(v.value[2] == 25000);
      CHECK(v.derivatives[0].x == 1000);
      CHECK(v.derivatives[0].y == 2000);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 1000);
      CHECK(v.derivatives[1].y == 3000);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5000);
      CHECK(v.derivatives[2].y == 1000);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector * const same mapping
      auto var_sum = var_vect * 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 41000);
      CHECK(v.value[1] == 61000);
      CHECK(v.value[2] == 25000);
      CHECK(v.derivatives[0].x == 1000);
      CHECK(v.derivatives[0].y == 2000);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 1000);
      CHECK(v.derivatives[1].y == 3000);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 5000);
      CHECK(v.derivatives[2].y == 1000);
      CHECK(v.derivatives[2].z == 0);
    }
  }

  SECTION("test_divide") {
    auto var_f = Variable<GetY>(f);
    auto var_g = Variable<GetY>(g);
    auto var_h = Variable<IdMap<X>>(
        [](X x) { return Scalar<X>{2 * x.z, X{.x = 0, .y = 0, .z = 2}}; });
    auto var_vect = Variable<GetY, 3>(vect);
    auto var_vect_ix = Variable<IdMap<X>, 3>([](X x) {
      return Vector<X, 3>{{x.z, x.x, x.y},
                          {X{.x = 0, .y = 0, .z = 1}, X{.x = 1, .y = 0, .z = 0},
                           X{.x = 0, .y = 1, .z = 0}}};
    });

    { // scalar / scalar same mapping
      auto var_sum = var_f / var_g;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value == 2.05);
      CHECK(v.derivatives.x == -2);
      CHECK(v.derivatives.y == -0.0025);
      CHECK(v.derivatives.z == 0);
    }

    { // scalar / scalar different mapping
      auto var_sum = var_f / var_h;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value, WithinRel(41. / 600., 0.01));
      CHECK_THAT(v.derivatives.x, WithinRel(1. / 600., 0.01));
      CHECK_THAT(v.derivatives.y, WithinRel(2. / 600., 0.01));
      CHECK_THAT(v.derivatives.z, WithinRel(-82. / 360000., 0.01));
    }

    { // scalar / const
      auto var_sum = var_f / 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value, WithinRel(41. / 1000., 0.01));
      CHECK_THAT(v.derivatives.x, WithinRel(1. / 1000., 0.01));
      CHECK_THAT(v.derivatives.y, WithinRel(2. / 1000., 0.01));
      CHECK_THAT(v.derivatives.z, WithinRel(0, 0.01));
    }

    { // const / scalar
      auto var_sum = 1000 / var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value, WithinRel(1000. / 41., 0.01));
      CHECK_THAT(v.derivatives.x, WithinRel(-1000. / 1681., 0.01));
      CHECK_THAT(v.derivatives.y, WithinRel(-2000. / 1681., 0.01));
      CHECK_THAT(v.derivatives.z, WithinRel(0, 0.01));
    }

    { // vector / vector same mapping
      auto var_sum = var_vect / var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK(v.value[0] == 1);
      CHECK(v.value[1] == 1);
      CHECK(v.value[2] == 1);
      CHECK(v.derivatives[0].x == 0);
      CHECK(v.derivatives[0].y == 0);
      CHECK(v.derivatives[0].z == 0);
      CHECK(v.derivatives[1].x == 0);
      CHECK(v.derivatives[1].y == 0);
      CHECK(v.derivatives[1].z == 0);
      CHECK(v.derivatives[2].x == 0);
      CHECK(v.derivatives[2].y == 0);
      CHECK(v.derivatives[2].z == 0);
    }

    { // vector / vector different mapping
      auto var_sum = var_vect / var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(41. / 300., 0.01));
      CHECK_THAT(v.value[1], WithinRel(61, 0.01));
      CHECK_THAT(v.value[2], WithinRel(25. / 20., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(1. / 300., 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(2. / 300., 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(-41. / 90000., 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(-60, 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(3, 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(0.25, 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(-1. / 80., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }

    { // scalar / vector different mapping
      auto var_sum = var_f / var_vect_ix;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(41. / 300., 0.01));
      CHECK_THAT(v.value[1], WithinRel(41, 0.01));
      CHECK_THAT(v.value[2], WithinRel(41. / 20., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(1. / 300., 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(2. / 300., 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(-41. / 90000., 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(-40, 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(2, 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(1. / 20., 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(-1. / 400., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }

    { // vector / scalar different mapping
      auto var_sum = var_vect_ix / var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(300. / 41., 0.01));
      CHECK_THAT(v.value[1], WithinRel(1. / 41., 0.01));
      CHECK_THAT(v.value[2], WithinRel(20. / 41., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(-300. / 1681., 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(-600. / 1681., 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(1. / 41., 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(40. / 1681., 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(-2. / 1681., 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(-20. / 1681., 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(1. / 1681., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }

    { // scalar / vector same mapping
      auto var_sum = var_f / var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(1, 0.01));
      CHECK_THAT(v.value[1], WithinRel(41. / 61., 0.01));
      CHECK_THAT(v.value[2], WithinRel(41. / 25., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(20. / 3721., 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(-1. / 3721., 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(-180. / 625., 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(9. / 625., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }

    { // vector / scalar same mapping
      auto var_sum = var_vect / var_f;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(1, 0.01));
      CHECK_THAT(v.value[1], WithinRel(61. / 41., 0.01));
      CHECK_THAT(v.value[2], WithinRel(25. / 41., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(-20. / 1681., 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(1. / 1681., 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(180. / 1681., 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(-9. / 1681., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }

    { // const / vector same mapping
      auto var_sum = 1000 / var_vect;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(1000. / 41., 0.01));
      CHECK_THAT(v.value[1], WithinRel(1000. / 61., 0.01));
      CHECK_THAT(v.value[2], WithinRel(1000. / 25., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(-1000. / 1681., 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(-2000. / 1681., 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(-1000. / 3721., 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(-3000. / 3721., 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(-5000. / 625., 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(-1000. / 625., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }

    { // vector / const same mapping
      auto var_sum = var_vect / 1000;
      auto v = var_sum(X{.x = 1, .y = 20, .z = 300});
      CHECK_THAT(v.value[0], WithinRel(41. / 1000., 0.01));
      CHECK_THAT(v.value[1], WithinRel(61. / 1000., 0.01));
      CHECK_THAT(v.value[2], WithinRel(25. / 1000., 0.01));
      CHECK_THAT(v.derivatives[0].x, WithinRel(1. / 1000., 0.01));
      CHECK_THAT(v.derivatives[0].y, WithinRel(2. / 1000., 0.01));
      CHECK_THAT(v.derivatives[0].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[1].x, WithinRel(1. / 1000., 0.01));
      CHECK_THAT(v.derivatives[1].y, WithinRel(3. / 1000., 0.01));
      CHECK_THAT(v.derivatives[1].z, WithinRel(0, 0.01));
      CHECK_THAT(v.derivatives[2].x, WithinRel(5. / 1000., 0.01));
      CHECK_THAT(v.derivatives[2].y, WithinRel(1. / 1000., 0.01));
      CHECK_THAT(v.derivatives[2].z, WithinRel(0, 0.01));
    }
  }

  SECTION("test_multiple_terms") {
    // TODO
  }
}
