#include <catch2/catch_test_macros.hpp>

#include "pscal/state.h"
#include "pscal/value.h"

namespace {
struct X {
  using value_t = double;
  value_t x, y;
};
} // namespace

TEST_CASE("value") {
  using namespace pscal;

  SECTION("add") {
    auto sa = Scalar<X>{-1, {.x = 1, .y = 2}};
    auto sb = Scalar<X>{-10, {.x = 10, .y = 20}};
    auto va = Vector<X, 2>{{-1, -2}, {X{.x = 1, .y = 2}, X{.x = 3, .y = 4}}};
    auto vb =
        Vector<X, 2>{{-10, -20}, {X{.x = 10, .y = 20}, X{.x = 30, .y = 40}}};
    double cst = -90000;

    { // scalar + const
      auto z = sa + cst;
      CHECK(z.value == -90001);
      CHECK(z.derivatives.x == 1);
      CHECK(z.derivatives.y == 2);
    }

    { // const + scalar
      auto z = cst + sb;
      CHECK(z.value == -90010);
      CHECK(z.derivatives.x == 10);
      CHECK(z.derivatives.y == 20);
    }

    { // scalar + scalar
      auto z = sa + sb;
      CHECK(z.value == -11);
      CHECK(z.derivatives.x == 11);
      CHECK(z.derivatives.y == 22);
    }

    { // vector + const
      auto z = va + cst;
      CHECK(z.value[0] == -90001);
      CHECK(z.value[1] == -90002);
      CHECK(z.derivatives[0].x == 1);
      CHECK(z.derivatives[0].y == 2);
      CHECK(z.derivatives[1].x == 3);
      CHECK(z.derivatives[1].y == 4);
    }

    { // const + vector
      auto z = cst + vb;
      CHECK(z.value[0] == -90010);
      CHECK(z.value[1] == -90020);
      CHECK(z.derivatives[0].x == 10);
      CHECK(z.derivatives[0].y == 20);
      CHECK(z.derivatives[1].x == 30);
      CHECK(z.derivatives[1].y == 40);
    }

    { // vector + vector
      auto z = va + vb;
      CHECK(z.value[0] == -11);
      CHECK(z.value[1] == -22);
      CHECK(z.derivatives[0].x == 11);
      CHECK(z.derivatives[0].y == 22);
      CHECK(z.derivatives[1].x == 33);
      CHECK(z.derivatives[1].y == 44);
    }

    { // scalar + vector
      auto z = sa + vb;
      CHECK(z.value[0] == -11);
      CHECK(z.value[1] == -21);
      CHECK(z.derivatives[0].x == 11);
      CHECK(z.derivatives[0].y == 22);
      CHECK(z.derivatives[1].x == 31);
      CHECK(z.derivatives[1].y == 42);
    }

    { // vector + scalar
      auto z = va + sb;
      CHECK(z.value[0] == -11);
      CHECK(z.value[1] == -12);
      CHECK(z.derivatives[0].x == 11);
      CHECK(z.derivatives[0].y == 22);
      CHECK(z.derivatives[1].x == 13);
      CHECK(z.derivatives[1].y == 24);
    }
  }

  SECTION("add must fail") {
    // TODO
  }

  SECTION("subtract") {
    auto sa = Scalar<X>{-1, {.x = 1, .y = 2}};
    auto sb = Scalar<X>{-10, {.x = 10, .y = 20}};
    auto va = Vector<X, 2>{{-1, -2}, {X{.x = 1, .y = 2}, X{.x = 3, .y = 4}}};
    auto vb =
        Vector<X, 2>{{-10, -20}, {X{.x = 10, .y = 20}, X{.x = 30, .y = 40}}};
    double cst = -90000;

    { // scalar - const
      auto z = sa - cst;
      CHECK(z.value == 89999);
      CHECK(z.derivatives.x == 1);
      CHECK(z.derivatives.y == 2);
    }

    { // const - scalar
      auto z = cst - sb;
      CHECK(z.value == -89990);
      CHECK(z.derivatives.x == -10);
      CHECK(z.derivatives.y == -20);
    }

    { // scalar - scalar
      auto z = sa - sb;
      CHECK(z.value == 9);
      CHECK(z.derivatives.x == -9);
      CHECK(z.derivatives.y == -18);
    }

    { // vector - const
      auto z = va - cst;
      CHECK(z.value[0] == 89999);
      CHECK(z.value[1] == 89998);
      CHECK(z.derivatives[0].x == 1);
      CHECK(z.derivatives[0].y == 2);
      CHECK(z.derivatives[1].x == 3);
      CHECK(z.derivatives[1].y == 4);
    }

    { // const - vector
      auto z = cst - vb;
      CHECK(z.value[0] == -89990);
      CHECK(z.value[1] == -89980);
      CHECK(z.derivatives[0].x == -10);
      CHECK(z.derivatives[0].y == -20);
      CHECK(z.derivatives[1].x == -30);
      CHECK(z.derivatives[1].y == -40);
    }

    { // vector - vector
      auto z = va - vb;
      CHECK(z.value[0] == 9);
      CHECK(z.value[1] == 18);
      CHECK(z.derivatives[0].x == -9);
      CHECK(z.derivatives[0].y == -18);
      CHECK(z.derivatives[1].x == -27);
      CHECK(z.derivatives[1].y == -36);
    }

    { // scalar - vector
      auto z = sa - vb;
      CHECK(z.value[0] == 9);
      CHECK(z.value[1] == 19);
      CHECK(z.derivatives[0].x == -9);
      CHECK(z.derivatives[0].y == -18);
      CHECK(z.derivatives[1].x == -29);
      CHECK(z.derivatives[1].y == -38);
    }

    { // vector - scalar
      auto z = va - sb;
      CHECK(z.value[0] == 9);
      CHECK(z.value[1] == 8);
      CHECK(z.derivatives[0].x == -9);
      CHECK(z.derivatives[0].y == -18);
      CHECK(z.derivatives[1].x == -7);
      CHECK(z.derivatives[1].y == -16);
    }
  }

  SECTION("multiply") {
    auto sa = Scalar<X>{-1, {.x = 1, .y = 2}};
    auto sb = Scalar<X>{-10, {.x = 10, .y = 20}};
    auto va = Vector<X, 2>{{-1, -2}, {X{.x = 1, .y = 2}, X{.x = 3, .y = 4}}};
    auto vb =
        Vector<X, 2>{{-10, -20}, {X{.x = 10, .y = 20}, X{.x = 30, .y = 40}}};
    double cst = -90000;

    { // scalar * const
      auto z = sa * cst;
      CHECK(z.value == 90000);
      CHECK(z.derivatives.x == -90000);
      CHECK(z.derivatives.y == -180000);
    }

    { // const * scalar
      auto z = cst * sb;
      CHECK(z.value == 900000);
      CHECK(z.derivatives.x == -900000);
      CHECK(z.derivatives.y == -1800000);
    }

    { // scalar * scalar
      auto z = sa * sb;
      CHECK(z.value == 10);
      CHECK(z.derivatives.x == -20);
      CHECK(z.derivatives.y == -40);
    }

    { // vector * const
      auto z = va * cst;
      CHECK(z.value[0] == 90000);
      CHECK(z.value[1] == 180000);
      CHECK(z.derivatives[0].x == -90000);
      CHECK(z.derivatives[0].y == -180000);
      CHECK(z.derivatives[1].x == -270000);
      CHECK(z.derivatives[1].y == -360000);
    }

    { // const * vector
      auto z = cst * vb;
      CHECK(z.value[0] == 900000);
      CHECK(z.value[1] == 1800000);
      CHECK(z.derivatives[0].x == -900000);
      CHECK(z.derivatives[0].y == -1800000);
      CHECK(z.derivatives[1].x == -2700000);
      CHECK(z.derivatives[1].y == -3600000);
    }

    { // vector * vector
      auto z = va * vb;
      CHECK(z.value[0] == 10);
      CHECK(z.value[1] == 40);
      CHECK(z.derivatives[0].x == -20);
      CHECK(z.derivatives[0].y == -40);
      CHECK(z.derivatives[1].x == -120);
      CHECK(z.derivatives[1].y == -160);
    }

    { // scalar * vector
      auto z = sa * vb;
      CHECK(z.value[0] == 10);
      CHECK(z.value[1] == 20);
      CHECK(z.derivatives[0].x == -20);
      CHECK(z.derivatives[0].y == -40);
      CHECK(z.derivatives[1].x == -50);
      CHECK(z.derivatives[1].y == -80);
    }

    { // vector * scalar
      auto z = va * sb;
      CHECK(z.value[0] == 10);
      CHECK(z.value[1] == 20);
      CHECK(z.derivatives[0].x == -20);
      CHECK(z.derivatives[0].y == -40);
      CHECK(z.derivatives[1].x == -50);
      CHECK(z.derivatives[1].y == -80);
    }
  }

  SECTION("divide") {
    auto sa = Scalar<X>{-1, {.x = 1, .y = 2}};
    auto sb = Scalar<X>{-10, {.x = 10, .y = 20}};
    auto va = Vector<X, 2>{{-1, -2}, {X{.x = 1, .y = 2}, X{.x = 3, .y = 4}}};
    auto vb =
        Vector<X, 2>{{-10, -20}, {X{.x = 10, .y = 20}, X{.x = 30, .y = 40}}};
    double cst = -2;

    { // scalar / const
      auto z = sa / cst;
      CHECK(z.value == 0.5);
      CHECK(z.derivatives.x == -0.5);
      CHECK(z.derivatives.y == -1);
    }

    { // const / scalar
      auto z = cst / sb;
      CHECK(z.value == 0.2);
      CHECK(z.derivatives.x == 0.2);
      CHECK(z.derivatives.y == 0.4);
    }

    { // scalar / scalar
      auto z = sa / sb;
      CHECK(z.value == 0.1);
      CHECK(z.derivatives.x == 0);
      CHECK(z.derivatives.y == 0);
    }

    { // vector / const
      auto z = va / cst;
      CHECK(z.value[0] == 0.5);
      CHECK(z.value[1] == 1);
      CHECK(z.derivatives[0].x == -0.5);
      CHECK(z.derivatives[0].y == -1);
      CHECK(z.derivatives[1].x == -1.5);
      CHECK(z.derivatives[1].y == -2);
    }

    { // const / vector
      auto z = cst / vb;
      CHECK(z.value[0] == 0.2);
      CHECK(z.value[1] == 0.1);
      CHECK(z.derivatives[0].x == 0.2);
      CHECK(z.derivatives[0].y == 0.4);
      CHECK(z.derivatives[1].x == 0.15);
      CHECK(z.derivatives[1].y == 0.2);
    }

    { // vector / vector
      auto z = va / vb;
      CHECK(z.value[0] == 0.1);
      CHECK(z.value[1] == 0.1);
      CHECK(z.derivatives[0].x == 0);
      CHECK(z.derivatives[0].y == 0);
      CHECK(z.derivatives[1].x == 0);
      CHECK(z.derivatives[1].y == 0);
    }

    { // scalar / vector
      auto z = sa / vb;
      CHECK(z.value[0] == 0.1);
      CHECK(z.value[1] == 0.05);
      CHECK(z.derivatives[0].x == 0);
      CHECK(z.derivatives[0].y == 0);
      CHECK(z.derivatives[1].x == 0.025);
      CHECK(z.derivatives[1].y == 0);
    }

    { // vector / scalar
      auto z = va / sb;
      CHECK(z.value[0] == 0.1);
      CHECK(z.value[1] == 0.2);
      CHECK(z.derivatives[0].x == 0);
      CHECK(z.derivatives[0].y == 0);
      CHECK(z.derivatives[1].x == -0.1);
      CHECK(z.derivatives[1].y == 0);
    }
  }
}
