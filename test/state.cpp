#include "pscal/state.h"
#include <array>
#include <catch2/catch_test_macros.hpp>

#include <iostream>

TEST_CASE("state") {
  SECTION("state declaration") {
    using namespace pscal;

    struct A {
      using value_t = double;
      value_t x;
      value_t y;
      value_t z[2];
    };

    STATIC_CHECK(pscal::State<A>);
    CHECK(size<A>() == 4);
    CHECK(size(A()) == 4);

    { // Initializing an aggregate with an ordinary initializer list
      A a = {1, 2, 3, 4};
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    { // Initializing an aggregate with designated initializers
      A a = {1, 2, 3, 4};
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    // do not allow heterogeneous structures
    struct B {
      using value_t = double;
      int x;
      int y;
      int z;
      double a;
    };

    STATIC_CHECK(not pscal::State<B>);

    struct C {
      using value_t = double;
      double a;
      int x;
      int y;
      int z;
    };

    STATIC_CHECK(not pscal::State<C>);
  }

  SECTION("as array") {
    using namespace pscal;

    struct A {
      using value_t = double;
      value_t x, y, z[2];
    };

    {
      A a{.x = 1, .y = 2, .z = {3, 4}};
      auto &arr = as_array(a);

      CHECK(arr[0] == 1);
      CHECK(arr[1] == 2);
      CHECK(arr[2] == 3);
      CHECK(arr[3] == 4);

      // on peut changer arr et voir les nouvelles valeurs dans a
      arr[0] += 10;
      CHECK(arr[0] == 11);
      CHECK(a.x == 11);
    }

    {
      A a{.x = 1, .y = 2, .z = {3, 4}};
      auto &arr = as_array<NativeArray>(a);

      CHECK(arr[0] == 1);
      CHECK(arr[1] == 2);
      CHECK(arr[2] == 3);
      CHECK(arr[3] == 4);

      // on peut changer arr et voir les nouvelles valeurs dans a
      arr[0] += 10;
      CHECK(arr[0] == 11);
      CHECK(a.x == 11);
    }

    {
      A a{.x = 1, .y = 2, .z = {3, 4}};
      auto &arr = as_array<std::array>(a);

      CHECK(arr[0] == 1);
      CHECK(arr[1] == 2);
      CHECK(arr[2] == 3);
      CHECK(arr[3] == 4);

      // on peut changer arr et voir les nouvelles valeurs dans a
      arr[0] += 10;
      CHECK(arr[0] == 11);
      CHECK(a.x == 11);

      // std::array have size() method
      CHECK(arr.size() == 4);
    }
  }

  SECTION("as state") {
    using namespace pscal;

    struct A {
      using value_t = double;
      value_t x, y, z[2];
    };

    auto arr = std::array<double, 4>{1, 2, 3, 4};

    auto &a = as_state<A>(arr);

    CHECK(a.x == 1);
    CHECK(a.y == 2);
    CHECK(a.z[0] == 3);
    CHECK(a.z[1] == 4);

    // on peut changer arr et voir les nouvelles valeurs dans a
    a.x += 10;
    CHECK(a.x == 11);
    CHECK(arr[0] == 11);

    // avec un array simple
    double arr_simple[4] = {1, 2, 3, 4};
    auto &b = as_state<A>(arr_simple);
    CHECK(b.x == 1);
    CHECK(b.y == 2);
    CHECK(b.z[0] == 3);
    CHECK(b.z[1] == 4);
    b.x += 10;
    CHECK(b.x == 11);
    CHECK(arr_simple[0] == 11);

    const double arr_const[4] = {1, 2, 3, 4};
    auto &c = as_state<A>(arr_const);
    CHECK(c.x == 1);
    CHECK(c.y == 2);
    CHECK(c.z[0] == 3);
    CHECK(c.z[1] == 4);
    // the following creates a copy
    auto d = as_state<A>({1, 2, 3, 4});
    CHECK(d.x == 1);
    CHECK(d.y == 2);
    CHECK(d.z[0] == 3);
    CHECK(d.z[1] == 4);
  }

  SECTION("detail") {
    using namespace pscal;

    struct A {
      double x, y, z[2];
    };

    STATIC_CHECK(detail::size<A, double>() == 4);
    STATIC_CHECK(detail::nb_attributes<A, double>() == 4);

    struct B {
      int x;
      int y;
      int z;
      double a;
    };

    STATIC_CHECK(detail::size<B, double>() == 3);
    STATIC_CHECK(detail::nb_attributes<B, double>() == 0);

    STATIC_CHECK(detail::size<B, int>() == 6);
    STATIC_CHECK(detail::nb_attributes<B, int>() == 3);
  }

  SECTION("generic as state") {
    using namespace pscal;

    struct A {
      using value_t = double;
      value_t x, y, z[2];
    };

    {
      std::array<double, 4> arr{1, 2, 3, 4};
      auto &a = as_state<A, std::array>(arr);
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    {
      NativeArray<double, 4> arr{1, 2, 3, 4};
      auto &a = as_state<A, NativeArray>(arr);
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    {
      double arr[4]{1, 2, 3, 4};
      auto &a = as_state<A, NativeArray>(arr);
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    {
      std::array<double, 4> arr{1, 2, 3, 4};
      auto &a = as_state<A>(arr);
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    {
      NativeArray<double, 4> arr{1, 2, 3, 4};
      auto &a = as_state<A>(arr);
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }

    {
      double arr[4]{1, 2, 3, 4};
      auto &a = as_state<A>(arr);
      CHECK(a.x == 1);
      CHECK(a.y == 2);
      CHECK(a.z[0] == 3);
      CHECK(a.z[1] == 4);
    }
  }
}
