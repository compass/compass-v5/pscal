#pragma once

#include <functional>
#include <type_traits>

#include "pscal/state.h"

namespace pscal {
namespace math {

template <State S1, State S2> auto operator+=(S1 &x, const S2 &y) {
  // ? static_assert(S1::size()==S2::size());
  auto &a = x.as_array();
  auto &b = y.as_array();
  for (auto i = 0; i < S1::size(); ++i)
    a[i] += b[i];
}

template <typename T, typename S>
auto operator+(const State<T, S> &x, const State<T, S> &y) {
  S res;
  auto &a = x.as_array();
  auto &b = y.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = a[i] + b[i];
  return res;
}

template <typename T, typename S>
auto operator*=(State<T, S> &x, const State<T, S> &y) {
  auto &a = x.as_array();
  auto &b = y.as_array();
  for (auto i = 0; i < S::size(); ++i)
    a[i] *= b[i];
}

template <typename T, typename S>
auto operator*(const State<T, S> &x, const State<T, S> &y) {
  S res;
  auto &a = x.as_array();
  auto &b = y.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = a[i] * b[i];
  return res;
}

template <typename T, typename S>
auto operator*=(State<T, S> &x, const typename std::common_type<T>::type &y) {
  auto &a = x.as_array();
  for (auto i = 0; i < S::size(); ++i)
    a[i] *= y;
}

template <typename T, typename S>
auto operator*(const typename std::common_type<T>::type &x,
               const State<T, S> &y) {
  S res;
  auto &b = y.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = x * b[i];
  return res;
}

template <typename T, typename S>
auto operator*(const State<T, S> &x,
               const typename std::common_type<T>::type &y) {
  S res;
  auto &a = x.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = a[i] * y;
  return res;
}

template <typename T, typename S>
auto operator/=(State<T, S> &x, const State<T, S> &y) {
  auto &a = x.as_array();
  auto &b = y.as_array();
  for (auto i = 0; i < S::size(); ++i)
    a[i] /= b[i];
}

template <typename T, typename S>
auto operator/(const State<T, S> &x, const State<T, S> &y) {
  S res;
  auto &a = x.as_array();
  auto &b = y.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = a[i] / b[i];
  return res;
}

template <typename T, typename S>
auto operator/=(State<T, S> &x, const typename std::common_type<T>::type &y) {
  auto &a = x.as_array();
  for (auto i = 0; i < S::size(); ++i)
    a[i] /= y;
}

template <typename T, typename S>
auto operator/(const typename std::common_type<T>::type &x,
               const State<T, S> &y) {
  S res;
  auto &b = y.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = x / b[i];
  return res;
}

template <typename T, typename S>
auto operator/(const State<T, S> &x,
               const typename std::common_type<T>::type &y) {
  S res;
  auto &a = x.as_array();
  auto &c = res.as_array();
  for (auto i = 0; i < S::size(); ++i)
    c[i] = a[i] / y;
  return res;
}

namespace func {

template <AnyState S> struct Variable {
  using value_t = typename S::value_t;
  using func_t = std::function<value_t(const S &, S &)>;
  func_t func;
  Variable(auto &obj) : func(obj) {}
};

template <typename S>
auto operator+(const Variable<S> &a, const Variable<S> &b) {
  typename Variable<S>::func_t res = [a, b](const S &x, S &dx) {
    S db{};
    auto &v_dx = dx.as_array();
    auto &v_db = db.as_array();
    auto res = a.func(x, dx) + b.func(x, db);
    for (auto i = 0; i < S::size(); ++i)
      v_dx[i] += v_db[i];
    return res;
  };
  return Variable<S>(res);
}

template <typename S>
auto operator*(const Variable<S> &a, const Variable<S> &b) {
  typename Variable<S>::func_t res = [a, b](const S &x, S &dx) {
    S da{};
    S db{};
    auto &v_dx = dx.as_array();
    auto &v_da = da.as_array();
    auto &v_db = db.as_array();
    auto s_a = a.func(x, da);
    auto s_b = b.func(x, db);
    auto res = s_a * s_b;
    for (auto i = 0; i < S::size(); ++i)
      v_dx[i] = s_a * v_db[i] + s_b * v_da[i];
    return res;
  };
  return Variable<S>(res);
}

template <typename S>
auto operator*(const Variable<S> &a, const typename S::value_t &b) {
  typename Variable<S>::func_t res = [a, b](const S &x, S &dx) {
    auto res = b * a.func(x, dx);
    auto &v_dx = dx.as_array();
    for (auto i = 0; i < S::size(); ++i)
      v_dx[i] *= b;
    return res;
  };
  return Variable<S>(res);
}

template <typename S>
auto operator*(const typename S::value_t &a, const Variable<S> &b) {
  return b * a;
}

/* template<typename T, typename S> */
/*   auto operator+( */
/*       std::function< T(const S&, S&) >, */
/*       std::function< T(const S&, S&) > */
/*       /1* std::function< T(const State<T,S>&, State<T,S>&) >,
 * *1/ */
/*       /1* std::function< T(const State<T,S>&, State<T,S>&) >
 * *1/ */
/*       /1* _type<T,S>::func, *1/ */
/*       /1* _type<T,S>::func *1/ */
/*   ) { */
/*     return 42; */
/*   } */

} // namespace func

/* template<typename T> */
/*   T& addin(T& v, const T& dv) { */
/*     using namespace pscal::state; */
/*     auto& x = as_array(v); */
/*     auto& dx = as_array(dv); */
/*     for (std::size_t i=0; i < size<T>(); ++i) */
/*       x[i] += dx[i]; */
/*     return v; */
/*   } */

/* template<typename T> */
/*   T& add(const T& v, const T& w, T& res) { */
/*     using namespace pscal::state; */
/*     auto& x = as_array(v); */
/*     auto& y = as_array(w); */
/*     auto& z = as_array(res); */
/*     for (std::size_t i=0; i < size<T>(); ++i) */
/*       z[i] = x[i] + y[i]; */
/*     return res; */
/*   } */

/* template<typename T> */
/*   T add(const T& v, const T& w) { */
/*     T res; */
/*     return add(v, w, res); */
/*   } */

//     namespace expr {
//
//       template<typename T, typename E>
//         struct Expression {
//           using struct_t = T;
//           T::value_t operator[](std::size_t i) const {
//             return static_cast<E const&>(*this)[i];
//           }
//         };
//
//       template<typename T, typename E>
//         T eval(Expression<T, E> const& obj) {
//           using namespace pscal::state;
//           T res;
//           auto& arr = as_array(res);
//           for (std::size_t i=0; i < size<T>(); ++i)
//             arr[i] = obj[i];
//           return res;
//         }
//
//       /* template<typename T> */
//       /*   struct Value : public Expression<T, Value<T>> { */
//       /*     T& value; */
//       /*     Value(T& val) : value(val) {} */
//       /*     T::value_t operator[](std::size_t i) const { return
//       pscal::as_array(value)[i]; } */
//       /*     T::value_t &operator[](std::size_t i) { return
//       pscal::as_array(value)[i]; } */
//       /*   }; */
//
//       template<typename T>
//         struct Value : public Expression<T, Value<T>> {
//           using array_t = std::array<typename T::value_t,
//           pscal::size<T>()>; array_t& array; Value(T& val) :
//           array(pscal::as_array(val)) {} T::value_t
//           operator[](std::size_t i) const { return array[i]; }
//           /* T::value_t &operator[](std::size_t i) { return array[i]; } */
//         };
//
//       template<typename T, typename E1, typename E2>
//         struct Sum : public Expression<T, Sum<T, E1, E2>> {
//           E1 const& left;
//           E2 const& right;
//           Sum(E1 const& a, E2 const& b) : left(a), right(b) {
//             static_assert( std::is_same<T, typename E1::struct_t>::value ==
//             true); static_assert( std::is_same<T, typename
//             E2::struct_t>::value == true);
//           }
//           T::value_t operator[](std::size_t i) const { return left[i] +
//           right[i]; }
//         };
//
//       template<typename T, typename E1, typename E2>
//         auto operator+(Expression<T, E1> const& a, Expression<T, E2> const&
//         b) {
//           return Sum<T, E1, E2>(*static_cast<const E1*>(&a),
//           *static_cast<const E2*>(&b));
//         }
//
//       template<typename T, typename E1, typename E2>
//         struct Multiplication : public Expression<T, Multiplication<T, E1,
//         E2>> {
//           E1 const& left;
//           E2 const& right;
//           Multiplication(E1 const& a, E2 const& b) : left(a), right(b) {
//             static_assert( std::is_same<T, typename E1::struct_t>::value ==
//             true); static_assert( std::is_same<T, typename
//             E2::struct_t>::value == true);
//           }
//           T::value_t operator[](std::size_t i) const { return left[i] *
//           right[i]; }
//         };
//
//       template<typename T, typename E1, typename E2>
//         auto operator*(Expression<T, E1> const& a, Expression<T, E2> const&
//         b) {
//           return Multiplication<T, E1, E2>(*static_cast<const E1*>(&a),
//           *static_cast<const E2*>(&b));
//         }
//
//     } // namespace expr

} // namespace math

} // namespace pscal
