#pragma once

#include <array>
#include <functional>
#include <type_traits>

#include "pscal/state.h"
#include "pscal/value.h"

namespace pscal {

/** Wrapper around function that allows arithmetic operators.
 *
 *  - wrapped function must be `State->Scalar` or `State->Vector`
 *  - the template parameter `M` is a StateMapping that transforms the state of
 *    the wrapped function into a reference state
 *  - Variable object is callable: it behave like the wrapped function but on
 *    reference state
 *  - arithmetic operators between Variable objects produce new Variable
 *    objects with automaticaly generated functions
 */
template <StateMapping M, std::size_t size = 0> struct Variable {
  template <State S>
  using Value = std::conditional<(size == 0), Scalar<S>, Vector<S, size>>::type;

  template <typename V>
  using Func = typename std::function<V(const typename V::state_t &)>;

  Func<Value<typename M::To>> _func;

  Variable(const Func<Value<typename M::To>> &func) : _func{func} {}
  Variable(const Variable &) = default;
  Variable(Variable &&) = default;

  /** Computes wrapped function but on reference state instead.
   */
  Value<typename M::From> operator()(const typename M::From &state) const {
    auto map = M(state);
    auto val = _func(map.v);
    return val.apply_map(map);
  }
};

/*--- ADD --------------------------------------------------------------*/

// add scalar variables with different mappings
template <StateMapping M1, StateMapping M2>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator+(const Variable<M1> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>>(
      [a, b](const From &state) { return a(state) + b(state); });
}

// add scalar variables with same mapping
template <StateMapping M>
auto operator+(const Variable<M> &a, const Variable<M> &b) {
  return Variable<M>(
      [a, b](const M::To &state) { return a._func(state) + b._func(state); });
}

// add scalar variable to constant
template <StateMapping M>
auto operator+(const Variable<M> &a, const typename M::From::value_t &b) {
  return Variable<M>([a, b](const M::To &state) { return a._func(state) + b; });
}
template <StateMapping M>
auto operator+(const typename M::From::value_t &a, const Variable<M> &b) {
  return b + a;
}

// add vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator+(const Variable<M1, size> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) + b(state); });
}

// add vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator+(const Variable<M, size> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) + b._func(state); });
}

// add scalar and vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator+(const Variable<M1> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) + b(state); });
}
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator+(const Variable<M1, size> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) + b(state); });
}

// add scalar and vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator+(const Variable<M, size> &a, const Variable<M> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) + b._func(state); });
}
template <StateMapping M, std::size_t size>
auto operator+(const Variable<M> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) + b._func(state); });
}

// add vector variable to constant
template <StateMapping M, std::size_t size>
auto operator+(const Variable<M, size> &a, const typename M::From::value_t &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) + b; });
}
template <StateMapping M, std::size_t size>
auto operator+(const typename M::From::value_t &a, const Variable<M, size> &b) {
  return b + a;
}

/*--- SUBTRACT ---------------------------------------------------------*/

// subtract scalar variables with different mappings
template <StateMapping M1, StateMapping M2>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator-(const Variable<M1> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>>(
      [a, b](const From &state) { return a(state) - b(state); });
}

// subtract scalar variables with same mapping
template <StateMapping M>
auto operator-(const Variable<M> &a, const Variable<M> &b) {
  return Variable<M>(
      [a, b](const M::To &state) { return a._func(state) - b._func(state); });
}

// subtract scalar variable to constant
template <StateMapping M>
auto operator-(const Variable<M> &a, const typename M::From::value_t &b) {
  return Variable<M>([a, b](const M::To &state) { return a._func(state) - b; });
}
template <StateMapping M>
auto operator-(const typename M::From::value_t &a, const Variable<M> &b) {
  return Variable<M>([a, b](const M::To &state) { return a - b._func(state); });
}

// subtract vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator-(const Variable<M1, size> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) - b(state); });
}

// subtract vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator-(const Variable<M, size> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) - b._func(state); });
}

// subtract scalar and vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator-(const Variable<M1> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) - b(state); });
}
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator-(const Variable<M1, size> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) - b(state); });
}

// subtract scalar and vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator-(const Variable<M, size> &a, const Variable<M> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) - b._func(state); });
}
template <StateMapping M, std::size_t size>
auto operator-(const Variable<M> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) - b._func(state); });
}

// subtract vector variable to constant
template <StateMapping M, std::size_t size>
auto operator-(const Variable<M, size> &a, const typename M::From::value_t &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) - b; });
}
template <StateMapping M, std::size_t size>
auto operator-(const typename M::From::value_t &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a - b._func(state); });
}

/*--- MULTIPLY ---------------------------------------------------------*/

// multiply scalar variables with different mappings
template <StateMapping M1, StateMapping M2>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator*(const Variable<M1> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>>(
      [a, b](const From &state) { return a(state) * b(state); });
}

// multiply scalar variables with same mapping
template <StateMapping M>
auto operator*(const Variable<M> &a, const Variable<M> &b) {
  return Variable<M>(
      [a, b](const M::To &state) { return a._func(state) * b._func(state); });
}

// multiply scalar variable to constant
template <StateMapping M>
auto operator*(const Variable<M> &a, const typename M::From::value_t &b) {
  return Variable<M>([a, b](const M::To &state) { return a._func(state) * b; });
}
template <StateMapping M>
auto operator*(const typename M::From::value_t &a, const Variable<M> &b) {
  return b * a;
}

// multiply vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator*(const Variable<M1, size> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) * b(state); });
}

// multiply vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator*(const Variable<M, size> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) * b._func(state); });
}

// multiply scalar and vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator*(const Variable<M1> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) * b(state); });
}
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator*(const Variable<M1, size> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) * b(state); });
}

// multiply scalar and vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator*(const Variable<M, size> &a, const Variable<M> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) * b._func(state); });
}
template <StateMapping M, std::size_t size>
auto operator*(const Variable<M> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) * b._func(state); });
}

// multiply vector variable to constant
template <StateMapping M, std::size_t size>
auto operator*(const Variable<M, size> &a, const typename M::From::value_t &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) * b; });
}
template <StateMapping M, std::size_t size>
auto operator*(const typename M::From::value_t &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a * b._func(state); });
}

/*--- DIVIDE -----------------------------------------------------------*/

// divide scalar variables with different mappings
template <StateMapping M1, StateMapping M2>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator/(const Variable<M1> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>>(
      [a, b](const From &state) { return a(state) / b(state); });
}

// divide scalar variables with same mapping
template <StateMapping M>
auto operator/(const Variable<M> &a, const Variable<M> &b) {
  return Variable<M>(
      [a, b](const M::To &state) { return a._func(state) / b._func(state); });
}

// divide scalar variable to constant
template <StateMapping M>
auto operator/(const Variable<M> &a, const typename M::From::value_t &b) {
  return Variable<M>([a, b](const M::To &state) { return a._func(state) / b; });
}
template <StateMapping M>
auto operator/(const typename M::From::value_t &a, const Variable<M> &b) {
  return Variable<M>([a, b](const M::To &state) { return a / b._func(state); });
}

// divide vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator/(const Variable<M1, size> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) / b(state); });
}

// divide vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator/(const Variable<M, size> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) / b._func(state); });
}

// divide scalar and vector variables with different mappings
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator/(const Variable<M1> &a, const Variable<M2, size> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) / b(state); });
}
template <StateMapping M1, StateMapping M2, std::size_t size>
  requires std::same_as<typename M1::From, typename M2::From>
auto operator/(const Variable<M1, size> &a, const Variable<M2> &b) {
  using From = typename M1::From;
  return Variable<IdMap<From>, size>(
      [a, b](const From &state) { return a(state) / b(state); });
}

// divide scalar and vector variables with same mapping
template <StateMapping M, std::size_t size>
auto operator/(const Variable<M, size> &a, const Variable<M> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) / b._func(state); });
}
template <StateMapping M, std::size_t size>
auto operator/(const Variable<M> &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) / b._func(state); });
}

// divide vector variable to constant
template <StateMapping M, std::size_t size>
auto operator/(const Variable<M, size> &a, const typename M::From::value_t &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a._func(state) / b; });
}
template <StateMapping M, std::size_t size>
auto operator/(const typename M::From::value_t &a, const Variable<M, size> &b) {
  return Variable<M, size>(
      [a, b](const M::To &state) { return a / b._func(state); });
}

} // namespace pscal
