/** @file state.h
 */  // required for doxygen adding functions

/** \mainpage pscal : Physical State CALculator
 *
 *  A library that defines
 *  - pscal::State
 *  - pscal::StateMapping
 *  - pscal::Scalar / pscal::Vector
 *  - pscal::Variable
 *
 */

#pragma once

#include <concepts>

namespace pscal {

namespace detail {

template <typename S, typename value_t>
consteval auto nb_attributes(auto... attributes) {
  if constexpr (requires { S{attributes...}; } == false)
    return sizeof...(attributes) - 1;
  else
    return nb_attributes<S, value_t>(attributes..., value_t());
}

template <typename S, typename value_t> consteval auto size() {
  return sizeof(S) / sizeof(value_t);
}

template <typename S, typename value_t> consteval auto is_aligned() {
  return sizeof(S) % sizeof(value_t) == 0;
}

} // namespace detail

/** Templated alias for native array.
 */
template <typename T, std::size_t n> using NativeArray = T[n];

/** Concept for a state structure
 *
 *  The struct must be "simple and uniform":
 *    - it has a class member `value_t` aliasing a numeric type (eg `double`)
 *    - all attributes have the type `value_t` or are arrays of this type
 *    - all attributes are public
 *
 *  Note that array attributes can be regular array (eg `double[3]`)
 *  or std::array (eg `std::array<double, 3>`).
 *
 *  Example:
 *  ```
 *  struct X {
 *    using value_t = double;  // mandatory
 *    value_t x, y, z[2];      // public attributes of type value_t
 *  };
 *  ```
 *
 *  ## State size
 *  A State has a static size: the number of values the struct is holding.
 *
 *  See `pscal::size()`.
 *
 *  Example:
 *  ```
 *  size<X>() == 4
 *  ```
 *
 *  ## State / array conversion
 *  A State can be viewed as an array of fixed size and conversely.
 *
 *  See `pscal::as_array()` and `pscal::as_state()`.
 *
 *  Example:
 *  ```
 *  auto x = X{.x=1, .y=2, .z={3, 4}};
 *  auto arr = as_array(x);  // -> array of 4 double
 *  auto y = as_state<X>(arr);  // -> X instance, referring to same memory as x
 *  ```
 */
template <typename S>
concept State = std::is_floating_point_v<typename S::value_t> &&
                detail::is_aligned<S, typename S::value_t>() &&
                detail::size<S, typename S::value_t>() ==
                    detail::nb_attributes<S, typename S::value_t>();

/** Size of a pscal::State struct
 */
template <State S> constexpr auto size() {
  return detail::size<S, typename S::value_t>();
}

/** Size of a pscal::State instance
 */
template <State S> auto size(const S &obj) { return size<S>(); }

/** Converts pscal::State into array-like
 */
template <template <typename, std::size_t> typename array_t = NativeArray,
          State S>
auto &as_array(S &state) {
  return *reinterpret_cast<array_t<typename S::value_t, size<S>()> *>(
      static_cast<S *>(&state));
}

/** Converts pscal::State into array-like
 */
template <template <typename, std::size_t> typename array_t = NativeArray,
          State S>
const auto &as_array(const S &state) {
  return *reinterpret_cast<const array_t<typename S::value_t, size<S>()> *>(
      static_cast<const S *>(&state));
}

/** Converts array-like into pscal::State
 */
template <State S, template <typename, std::size_t> typename array_t>
S &as_state(array_t<typename S::value_t, size<S>()> &arr) {
  return *reinterpret_cast<S *>(
      static_cast<array_t<typename S::value_t, size<S>()> *>(&arr));
}

/** Converts array-like into pscal::State
 */
template <State S, template <typename, std::size_t> typename array_t>
const S &as_state(const array_t<typename S::value_t, size<S>()> &arr) {
  return *reinterpret_cast<const S *>(
      static_cast<const array_t<typename S::value_t, size<S>()> *>(&arr));
}

/** Converts native array into pscal::State
 */
template <State S> S &as_state(typename S::value_t (&arr)[size<S>()]) {
  return *reinterpret_cast<S *>(
      static_cast<typename S::value_t(*)[size<S>()]>(&arr));
}

/** Converts native array into pscal::State
 */
template <State S>
const S &as_state(const typename S::value_t (&arr)[size<S>()]) {
  return *reinterpret_cast<const S *>(
      static_cast<const typename S::value_t(*)[size<S>()]>(&arr));
}

/** Concept for state mapping
 *
 *  A state mapping encodes the mapping between two types of pscal::State.
 *
 *  Let consider two types of state X and Y, and the mapping m from X to Y:
 *  ```
 *    y = m(x) for x in X and y in Y
 *  ```
 *  A function f on Y can be composed with m, f°m(x) = f(m(x)), to define a
 *  function on X. The derivative of f°m is given by:
 *  ```
 *    d( f°m )          d m       d f
 *    -------- (x)  =   --- (x) . --- (m(y))
 *      d X             d X       d Y
 *  ```
 *
 *  A StateMapping object `map`
 *    - is created from a X instance : `map = M(x)`
 *    - has a `map.v` attribute of type Y, holding the value of y=m(x)
 *    - has a method `map.d(dfdy)` that apply dm/dx on df/dy to get df/dx
 *  A StateMapping class `M`
 *    - has a class member `From` aliasing the type X
 *    - has a class member `To` aliasing the type Y
 */
template <typename M>
concept StateMapping = State<typename M::From> && State<typename M::To> &&
                       requires(M map, M::From from, M::To d_to) {
                         { M(from) } -> std::same_as<M>;
                         { map.v } -> std::same_as<typename M::To &>;
                         { map.d(d_to) } -> std::same_as<typename M::From>;
                       };

/** A StateMapping for the identity mapping.
 */
template <State S> struct IdMap {
  using From = S;
  using To = S;
  S v;
  IdMap(const S &state) : v{state} {}
  S d(const S &state) { return state; }
};

} // namespace pscal
