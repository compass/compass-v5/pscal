#pragma once

#include "pscal/state.h"
#include <cassert>

namespace pscal {
namespace multistate {

template <typename V>
concept is_vector_of_states = State<typename V::value_type>;

template <typename T>
concept Scalar =
    is_vector_of_states<decltype(T::derivatives)> &&
    std::same_as<typename decltype(T::derivatives)::value_type::value_t,
                 decltype(T::value)>;

template <Scalar T> constexpr auto size() {
  return pscal::size<typename decltype(T::derivatives)::value_type>();
}

namespace operators {

template <Scalar T> T &operator+=(T &a, const T &b) {
  auto N = a.derivatives.size();
  assert(b.derivatives.size() == N);
  a.value += b.value;
  for (auto i = 0; i < N; ++i) {
    auto da = as_array(a.derivatives[i]);
    auto db = as_array(b.derivatives[i]);
    for (auto j = 0; j < size<T>(); ++j)
      da[j] += db[j];
  }
  return a;
}

template <Scalar T> T &operator-=(T &a, const T &b) {
  auto N = a.derivatives.size();
  assert(b.derivatives.size() == N);
  a.value -= b.value;
  for (auto i = 0; i < N; ++i) {
    auto da = as_array(a.derivatives[i]);
    auto db = as_array(b.derivatives[i]);
    for (auto j = 0; j < size<T>(); ++j)
      da[j] -= db[j];
  }
  return a;
}

template <Scalar T> T &operator*=(T &a, const decltype(T::value) &b) {
  auto N = a.derivatives.size();
  a.value *= b;
  for (auto i = 0; i < N; ++i) {
    auto da = as_array(a.derivatives[i]);
    for (auto j = 0; j < size<T>(); ++j)
      da[j] *= b;
  }
  return a;
}

} // namespace operators
} // namespace multistate
} // namespace pscal
