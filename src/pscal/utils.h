#pragma once

#include <iostream>

#include "pscal/state.h"

namespace pscal {
namespace utils {

void print(auto const &obj) {
  using namespace pscal;
  auto &arr = as_array(obj);
  std::cout << typeid(obj).name() << " ";
  for (std::size_t i = 0; i < size(obj); i++)
    std::cout << arr[i] << " ";
  std::cout << "\n";
}

} // namespace utils

} // namespace pscal
