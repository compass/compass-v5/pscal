#pragma once

#include <array>
#include <concepts>

#include "pscal/state.h"

namespace pscal {

/** Scalar value and its derivatives over the state S.
 *  Arithmetic operators are defined to propagate derivatives.
 */
template <State S> struct Scalar {
  using state_t = S;

  S::value_t value;
  S derivatives;

  explicit operator typename S::value_t() const { return value; }

  /** Applies a StateMapping to get the same value but with derivatives on a new
   *  State.
   */
  template <StateMapping M>
    requires std::same_as<S, typename M::To>
  Scalar<typename M::From> apply_map(M map) {
    return {value, map.d(derivatives)};
  }
};

/** Vector value and its derivatives over the state S.
 *  Arithmetic operators are defined to propagate derivatives.
 */
template <State S, std::size_t size> struct Vector {
  using state_t = S;

  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;

  Scalar<S> operator[](const std::size_t i) const {
    return {value[i], derivatives[i]};
  }

  /** Applies a StateMapping to get the same value but with derivatives on a new
   * State.
   */
  template <StateMapping M>
    requires std::same_as<S, typename M::To>
  Vector<typename M::From, size> apply_map(M map) {
    std::array<typename M::From, size> new_derivatives;
    for (auto i = 0; i < size; ++i)
      new_derivatives[i] = map.d(derivatives[i]);
    return {value, new_derivatives};
  }
};

/*--- ADD --------------------------------------------------------------*/

// scalar + const
template <State S>
auto operator+(const Scalar<S> &a, const typename S::value_t &b) {
  return Scalar<S>{a.value + b, a.derivatives};
}

// const + scalar
template <State S>
auto operator+(const typename S::value_t &a, const Scalar<S> &b) {
  return b + a;
}

// scalar + scalar
template <State S> auto operator+(const Scalar<S> &a, const Scalar<S> &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_da = as_array(a.derivatives);
  auto &arr_db = as_array(b.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = arr_da[i] + arr_db[i];
  return Scalar<S>{a.value + b.value, derivatives};
}

// vector + const
template <State S, std::size_t size>
auto operator+(const Vector<S, size> &a, const typename S::value_t &b) {
  std::array<typename S::value_t, size> value;
  for (auto k = 0; k < size; ++k)
    value[k] = a.value[k] + b;
  return Vector<S, size>{value, a.derivatives};
}

// const + vector
template <State S, std::size_t size>
auto operator+(const typename S::value_t &a, const Vector<S, size> &b) {
  return b + a;
}

// vector + vector
template <State S, std::size_t size>
auto operator+(const Vector<S, size> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] + b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] + arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// scalar + vector
template <State S, std::size_t size>
auto operator+(const Scalar<S> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  auto &arr_da = as_array(a.derivatives);
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value + b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] + arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// vector + scalar
template <State S, std::size_t size>
auto operator+(const Vector<S, size> &a, const Scalar<S> &b) {
  return b + a;
}

/*--- SUBTRACT ---------------------------------------------------------*/

// scalar - const
template <State S>
auto operator-(const Scalar<S> &a, const typename S::value_t &b) {
  return Scalar<S>{a.value - b, a.derivatives};
}

// const - scalar
template <State S>
auto operator-(const typename S::value_t &a, const Scalar<S> &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_db = as_array(b.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = -arr_db[i];
  return Scalar<S>{a - b.value, derivatives};
}

// scalar - scalar
template <State S> auto operator-(const Scalar<S> &a, const Scalar<S> &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_da = as_array(a.derivatives);
  auto &arr_db = as_array(b.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = arr_da[i] - arr_db[i];
  return Scalar<S>{a.value - b.value, derivatives};
}

// vector - const
template <State S, std::size_t size>
auto operator-(const Vector<S, size> &a, const typename S::value_t &b) {
  std::array<typename S::value_t, size> value;
  for (auto k = 0; k < size; ++k)
    value[k] = a.value[k] - b;
  return Vector<S, size>{value, a.derivatives};
}

// const - vector
template <State S, std::size_t size>
auto operator-(const typename S::value_t &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a - b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = -arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// vector - vector
template <State S, std::size_t size>
auto operator-(const Vector<S, size> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] - b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] - arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// scalar - vector
template <State S, std::size_t size>
auto operator-(const Scalar<S> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  auto &arr_da = as_array(a.derivatives);
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value - b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] - arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// vector - scalar
template <State S, std::size_t size>
auto operator-(const Vector<S, size> &a, const Scalar<S> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  auto &arr_db = as_array(b.derivatives);
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] - b.value;
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] - arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

/*--- MULTIPLY ---------------------------------------------------------*/

// scalar * const
template <State S>
auto operator*(const Scalar<S> &a, const typename S::value_t &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_da = as_array(a.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = arr_da[i] * b;
  return Scalar<S>{a.value * b, derivatives};
}

// const * scalar
template <State S>
auto operator*(const typename S::value_t &a, const Scalar<S> &b) {
  return b * a;
}

// scalar * scalar
template <State S> auto operator*(const Scalar<S> &a, const Scalar<S> &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_da = as_array(a.derivatives);
  auto &arr_db = as_array(b.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = arr_da[i] * b.value + a.value * arr_db[i];
  return Scalar<S>{a.value * b.value, derivatives};
}

// vector * const
template <State S, std::size_t size>
auto operator*(const Vector<S, size> &a, const typename S::value_t &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    value[k] = a.value[k] * b;
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] * b;
  }
  return Vector<S, size>{value, derivatives};
}

// const * vector
template <State S, std::size_t size>
auto operator*(const typename S::value_t &a, const Vector<S, size> &b) {
  return b * a;
}

// vector * vector
template <State S, std::size_t size>
auto operator*(const Vector<S, size> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] * b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] * b.value[k] + a.value[k] * arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// scalar * vector
template <State S, std::size_t size>
auto operator*(const Scalar<S> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  auto &arr_da = as_array(a.derivatives);
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value * b.value[k];
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] * b.value[k] + a.value * arr_db[i];
  }
  return Vector<S, size>{value, derivatives};
}

// vector * scalar
template <State S, std::size_t size>
auto operator*(const Vector<S, size> &a, const Scalar<S> &b) {
  return b * a;
}

/*--- DIVIDE -----------------------------------------------------------*/

// scalar / const
template <State S>
auto operator/(const Scalar<S> &a, const typename S::value_t &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_da = as_array(a.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = arr_da[i] / b;
  return Scalar<S>{a.value / b, derivatives};
}

// const / scalar
template <State S>
auto operator/(const typename S::value_t &a, const Scalar<S> &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_db = as_array(b.derivatives);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = -a / (b.value * b.value) * arr_db[i];
  return Scalar<S>{a / b.value, derivatives};
}

// scalar / scalar
template <State S> auto operator/(const Scalar<S> &a, const Scalar<S> &b) {
  S derivatives;
  auto &arr_dr = as_array(derivatives);
  auto &arr_da = as_array(a.derivatives);
  auto &arr_db = as_array(b.derivatives);
  auto scale = 1 / (b.value * b.value);
  for (auto i = 0; i < size<S>(); ++i)
    arr_dr[i] = (arr_da[i] * b.value - a.value * arr_db[i]) * scale;
  return Scalar<S>{a.value / b.value, derivatives};
}

// vector / const
template <State S, std::size_t size>
auto operator/(const Vector<S, size> &a, const typename S::value_t &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] / b;
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = arr_da[i] / b;
  }
  return Vector<S, size>{value, derivatives};
}

// const / vector
template <State S, std::size_t size>
auto operator/(const typename S::value_t &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a / b.value[k];
    auto scale = 1 / (b.value[k] * b.value[k]);
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = (-a * arr_db[i]) * scale;
  }
  return Vector<S, size>{value, derivatives};
}

// vector / vector
template <State S, std::size_t size>
auto operator/(const Vector<S, size> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] / b.value[k];
    auto scale = 1 / (b.value[k] * b.value[k]);
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = (arr_da[i] * b.value[k] - a.value[k] * arr_db[i]) * scale;
  }
  return Vector<S, size>{value, derivatives};
}

// scalar / vector
template <State S, std::size_t size>
auto operator/(const Scalar<S> &a, const Vector<S, size> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  auto &arr_da = as_array(a.derivatives);
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value / b.value[k];
    auto scale = 1 / (b.value[k] * b.value[k]);
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_db = as_array(b.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = (arr_da[i] * b.value[k] - a.value * arr_db[i]) * scale;
  }
  return Vector<S, size>{value, derivatives};
}

// vector / scalar
template <State S, std::size_t size>
auto operator/(const Vector<S, size> &a, const Scalar<S> &b) {
  std::array<typename S::value_t, size> value;
  std::array<S, size> derivatives;
  auto &arr_db = as_array(b.derivatives);
  auto scale = 1 / (b.value * b.value);
  for (auto k = 0; k < size; ++k) {
    value[k] = a.value[k] / b.value;
    auto &arr_dr = as_array(derivatives[k]);
    auto &arr_da = as_array(a.derivatives[k]);
    for (auto i = 0; i < pscal::size<S>(); ++i)
      arr_dr[i] = (arr_da[i] * b.value - a.value[k] * arr_db[i]) * scale;
  }
  return Vector<S, size>{value, derivatives};
}

} // namespace pscal
